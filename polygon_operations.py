# import libs for plotting and operations with polygons
from shapely.geometry import Polygon
import matplotlib.pyplot as plot
import descartes


if __name__ == '__main__':
    # Create rectangle like in task
    A = Polygon([(0, 1), (0, 3), (4, 3), (4, 1)])
    B = Polygon([(1, 0), (1, 5), (3, 5), (3, 0)])
    # Using shapely library to realize OR, AND, MINUS funcs for polygons
    # Create A OR B
    C = A.union(B)
    # Create A AND B
    D = A.intersection(B)
    # Create C Minus D - solve of the task
    E = C.difference(D)
    # Draw plot
    ax = plot.gca()
    ax.add_patch(descartes.PolygonPatch(E, fc='b', ec='k', alpha=0.2))
    ax.set_xlim(-2, 10)
    ax.set_ylim(-2, 10)
    ax.set_aspect('equal')
    plot.show()
