import re
import matplotlib.pyplot as plot
from matplotlib.patches import Polygon as Pol, Rectangle as Rec
from python_questions import *


class DrawingApp:
    polygons = list()
    rectangles = list()

    def add_polygon(self, polygon):
        self.polygons.append(polygon)

    def add_rectangle(self, rectangle):
        self.rectangles.append(rectangle)

    def draw_plot(self, coords):
        fig, ax = plot.subplots()
        if len(self.rectangles) != 0:
            for i in range(len(self.rectangles)):
                ax.add_patch(Rec((self.rectangles[i].left, self.rectangles[i].bottom),
                             self.rectangles[i].right - self.rectangles[i].left,
                             self.rectangles[i].top - self.rectangles[i].bottom,
                             facecolor=self.rectangles[i].color))
                print(f'Rectangle\nLeft = {self.rectangles[i].left}\nRight = {self.rectangles[i].right}\n'
                      f'Top = {self.rectangles[i].top}\nBottom = {self.rectangles[i].bottom}\n'
                      f'Width = {self.rectangles[i].right - self.rectangles[i].left}\n'
                      f'Height = {self.rectangles[i].top - self.rectangles[i].bottom}\n'
                      f'Color = {self.rectangles[i].color} ')
        if len(self.polygons) != 0:
            for i in range(len(self.polygons)):
                ax.add_patch(Pol(self.polygons[i].points, color=self.polygons[i].color))
                print(f'Polygon\n'
                      f'Points = {self.polygons[i].points}\n'
                      f'Colors = {self.polygons[i].color}')

        plot.axis(coords)
        plot.show()


if __name__ == '__main__':
    draw = DrawingApp()
    colors = ('black', 'red', 'blue', 'green', 'cyan', 'magenta', 'yellow')
    while True:
        com = input('Commands - add_polygon, add_rectangle, show_plot, exit:')
        if com == 'exit':
            break
        elif com == 'add_polygon':
            polygon_color = input(f'Enter polygon color. Colors = {colors}:')
            get_points = input('Enter polygon points.Sides must be horizontal or'
                               ' vertical. Example, (1,2),(1,5),(4,5),(4,4),(3,4),(3,2):')
            try:
                get_numbers = re.findall('(\-?\d+\.*\d*\,\-?\d+\.*\d*)', get_points)
                polygon_points = tuple((float(get_numbers[i].split(',')[0]), float(get_numbers[i].split(',')[1]))
                                       for i in range(len(get_numbers)))
                new_polygon = Polygon(polygon_color, polygon_points)
                if new_polygon.is_correct and polygon_color in colors:
                    draw.add_polygon(new_polygon)
                else:
                    del new_polygon
                    print('Wrong data.')
            except Exception as e:
                print(e)
        elif com == 'add_rectangle':
            rectangle_color = input(f'Enter rectangle color. Colors = {colors}:')
            rectangle_left = input('Enter left side of rectangle:')
            rectangle_right = input('Enter right side of rectangle:')
            rectangle_top = input('Enter top side of rectangle:')
            rectangle_bottom = input('Enter bottom side of rectangle:')
            new_rectangle = Rectangle(rectangle_color, float(rectangle_left), float(rectangle_right),
                                      float(rectangle_top), float(rectangle_bottom))
            if new_rectangle.is_correct and rectangle_color in colors:
                draw.add_rectangle(new_rectangle)
            else:
                del new_rectangle
                print('Wrong data.')
        elif com == 'show_plot':
            plots = input('Enter coordinates of min and max x,y. Format - -100 100 -100 100 :')
            try:
                get_inputs = re.findall('(\-?\d+\.*\d*)', plots)
                coords = list(float(coord) for coord in get_inputs)
                draw.draw_plot(coords)
            except Exception as e:
                print(e)
        else:
            print("Error command.Try again")
