
class Rectangle:
    color = None
    left = None
    right = None
    top = None
    bottom = None
    is_correct = True

    def __init__(self, color='black', *args):
        is_numbs = True
        self.color = color
        for arg in args:
            if type(arg) is not int and type(arg) is not float:
                is_numbs = False
        if len(args) == 4 and is_numbs and args[0] < args[1] and args[2] > args[3]:
            self.left, self.right, self.top, self.bottom = args
        else:
            print('Error by creating Rectangle. Sides are not correct')
            self.is_correct = False


class Polygon:
    color = None
    points = None
    is_correct = True

    def __init__(self, color='black', points=None):
        is_correct = True
        self.color = color
        try:
            for i in range(len(points)-1):
                if points[i][0] != points[i+1][0] and points[i][1] != points[i+1][1]:
                    is_correct = False
        except Exception as e:
            print('Error by creating Polygon. ' + e)
        if is_correct:
            self.points = points
        else:
            print('Error by creating Polygon.Points are not correct')
            self.is_correct = False


# parameters to draw rectangle is define in class Rectangle, so we need to pass Rectangle object to draw it
def draw_rectangle(rectangle):
    pass


# parameters to draw polygon is define in class Polygon, so we need to pass Polygon object to draw it
def draw_polygon(polygon):
    pass
